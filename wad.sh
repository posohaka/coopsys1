#!/bin/bash
chmod 777 ./builder.sh
while true; do
  find . \( -name .idea -o -name .m2 -o -name target \) -prune -o -name "*.java" -o -name "*.xml" -o -name "*.scss" -o -name "*.coffee" -o -name "*.jsp" | entr ./builder.sh
done
