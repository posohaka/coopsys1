FROM maven:3.6.3-jdk-11-slim

RUN apt-get update && apt-get install -y npm entr

RUN npm install -g sass coffeescript

RUN coffee --version
RUN sass --version

