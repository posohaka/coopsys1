all: run

clean:
	@docker-compose -f docker-compose.yml down

rm:
	@docker-compose -f docker-compose.yml down -v

clean_test:
	@docker-compose -f docker-compose.test.yml run test_builder mvn clean -B -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn
	@docker-compose -f docker-compose.test.yml down

test:
	@docker-compose -f docker-compose.test.yml build
	@docker-compose -f docker-compose.test.yml run test_builder mvn clean test -B -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn

build:
	@docker-compose build

run:
	@docker-compose -f docker-compose.yml up
