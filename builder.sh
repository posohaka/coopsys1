#!/bin/bash

function compile_sass() {
  echo "******* Compiling sass *******"
  sass --style=compressed /app/src/main/webapp/static/scss:/app/src/main/webapp/static/css
}

function compile_coffee() {
  echo "**** Compiling coffescript *****"
  coffee --compile --output /app/src/main/webapp/static/js /app/src/main/webapp/static/coffee
}

function package_war() {
  echo "******* Building maven *********"
  mvn clean install -DskipTests=true
  #rm -rfv /tomcat_wars/*
  echo "********* Copying war **********"
  cp /app/target/corporative_systems.war /tomcat_wars/ROOT.war

}

function main() {
  compile_sass
  compile_coffee
  package_war
  echo "******** WAR updated **********"
}

main
