<%@ page import="java.util.List" %>
<%@ page import="com.s3ai.entities.Book" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Books list</title>
    <%@include file='../templates/imports.jsp' %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/list.css">
</head>
<body>
<%@include file='../templates/header.jsp' %>

<div class="container-fluid image-wrapper"
     style="background: url('${pageContext.request.contextPath}/static/images/books_list.jpg')">
    <div class="row justify-content-center">
        <a class="btn btn-primary btn-lg active" style="width: 100%; padding-top: 63px" role="button" aria-pressed="true" href="${pageContext.request.contextPath}/book">Create New</a>
        <div class="pre-scrollable row justify-content-center table-content">
            <table class="table col-4 table-hover table-striped table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">User</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<Book> books = (List<Book>) request.getSession().getAttribute("books");
                    for (int i = 0; i < books.size(); i++) {
                        Book book = books.get(i);
                        out.println("<tr book_id='" + book.getId().toString() + "'>");
                        out.println("<td  scope=\"col\">" + (i + 1) + "</td>");
                        out.println("<td attr_name=\"name\">" + (book.getName() == null ? "" : book.getName()) + "</td>");
                        out.println("<td attr_name=\"user\">" + (book.getUser() == null ? "" : book.getUser().getName() + " (" + book.getUser().getAge()) + ")</td>");
                        out.println("</tr>");
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="bookModal" tabindex="-1" role="dialog"
         aria-labelledby="bookModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Book info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row justify-content-between">
                            <p>ID:</p>
                            <p id="modal_id"></p>
                        </div>
                        <div class="d-flex flex-row justify-content-between">
                            <p>Name:</p>
                            <p id="modal_name"></p>
                        </div>
                        <div class="d-flex flex-row justify-content-between">
                            <p>User:</p>
                            <p id="modal_user_id">value</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="edit_book" class="btn btn-primary"><i class="fas fa-edit"></i> Edit
                        book
                    </button>
                    <button type="button" id="delete_book" class="btn btn-danger"><i class="fas fa-trash-alt"></i>
                        Delete book
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    let context_url = "${pageContext.request.contextPath}"
</script>
<script src="${pageContext.request.contextPath}/static/js/books.js"></script>
</html>
