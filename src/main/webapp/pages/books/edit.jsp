<%@ page import="com.s3ai.entities.Book" %>
<%@ page import="com.s3ai.entities.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit book</title>
    <%@include file='../templates/imports.jsp' %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/list.css">
    <script src="${pageContext.request.contextPath}/static/js/books.js"></script>
</head>
<body>
<%
    Book book = (Book) request.getSession().getAttribute("book");
    String id = book.getId() == null ? "" : book.getId().toString();
    String name = book.getName() == null ? "" : book.getName();
    String userId = book.getUser() == null ? "" : book.getUser().getId().toString();
    List<User> users = (List<User>) request.getSession().getAttribute("users");
%>

<!-- Navigation -->
<%@include file='../templates/header.jsp' %>
<div class="editing image-wrapper form-center"
     style="background: url('${pageContext.request.contextPath}/static/images/new_book.jpg')">
    <div class="container justify-content-center">

        <form id="bookForm" class="d-flex justify-content-center" method="post"
              action="${pageContext.request.contextPath}/book">
            <fieldset>
                <input type="hidden" name="id" value="<%out.print(id);%>">
                <div class="form-group" style="margin: 0 auto">
                    <div class="inputGroupContainer">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-book"></i></div>
                            </div>
                            <input type="text"
                                   class="form-control validate"
                                   id="bookName"
                                   name="name"
                                   placeholder="Book Name" value="<%out.println(name);%>" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="inputGroupContainer">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-user-circle"></i></div>
                            </div>
                            <select class="form-control" id="userSelect" name="userId" required>
                                <option value="">User</option>
                                <%
                                    for (User user : users) {
                                        if (user.getId().toString().equals(userId)) {
                                            out.println("<option value=\"" + user.getId().toString() + "\" selected>" + user.getName() + " (" + user.getAge() + ")</option>");

                                        } else {
                                            out.println("<option value=\"" + user.getId().toString() + "\">" + user.getName() + " (" + user.getAge() + ")</option>");
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="justify-content-center">
                    <button type="submit" class="btn btn-primary w-100"><i class="fas fa-plus"></i> Create</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>
