<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/fontawesome/css/all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/jquery.toast.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/project.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-3.4.1.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.toast.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
