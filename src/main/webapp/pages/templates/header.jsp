<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-dark navbar-expand-lg bg-dark fixed-top mainNavigationHeader" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="${pageContext.request.contextPath}/">Book Store System</a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/books" id="navbarDropdownBook" role="button">
                        Books
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/users" id="navbarDropdownUser" role="button">
                        Users
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
