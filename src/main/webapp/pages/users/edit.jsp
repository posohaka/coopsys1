<%@ page import="com.s3ai.entities.User" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit user</title>
    <%@include file='../templates/imports.jsp' %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/list.css">
    <script src="${pageContext.request.contextPath}/static/js/books.js"></script>
</head>
<body>
<%
    User user = (User) request.getSession().getAttribute("user");
    String id = user.getId() == null ? "" : user.getId().toString();
    String name = user.getName() == null ? "" : user.getName();
    Integer age = user.getAge();
%>

<!-- Navigation -->
<%@include file='../templates/header.jsp' %>
<div class="editing image-wrapper"
     style="background: url('${pageContext.request.contextPath}/static/images/new_user.jpg')">
    <div class="container justify-content-center">

        <form id="userForm" class="d-flex justify-content-center" method="post"
              action="${pageContext.request.contextPath}/user">
            <fieldset>
                <input type="hidden" name="id" value="<%out.print(id);%>">
                <div class="form-group">
                    <div class="inputGroupContainer">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-user-circle"></i></div>
                            </div>
                            <input type="text"
                                   class="form-control validate"
                                   id="userName"
                                   name="name"
                                   placeholder="Users's name" value="<%out.println(name);%>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="inputGroupContainer">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="far fa-calendar"></i></div>
                            </div>
                            <input type="number"
                                   min="0"
                                   class="form-control validate"
                                   id="userAge"
                                   name="age"
                                   placeholder="User's age"
                                   value="<%out.print(age);%>" required>
                        </div>
                    </div>
                </div>
                <div class="justify-content-center">
                    <button type="submit" class="btn btn-primary w-100"><i class="fas fa-plus"></i> Create</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>
