fill_modal = (event) ->
  target = $(this)
  book_id = target.attr 'book_id'
  if undefined != book_id and "" != book_id
    $("#modal_id").text book_id
    $("#modal_name").text target.find("[attr_name='name']").text()
    $("#modal_user_id").text target.find("[attr_name='user_id']").text()
    $("#bookModal").modal('toggle')

hrefToEdit = (event) ->
  book_id = $("#modal_id").text()
  window.location.replace "#{context_url}/book/?id=#{book_id}"

removeBook = (event) ->
  book_id = $("#modal_id").text()
  $.ajax
    url: "#{context_url}/book/?id=#{book_id}",
    type: 'DELETE'
    error: (jqXHR, textStatus, errorThrown) ->
      $.toast
        icon: 'error',
        hideAfter: 5000,
        heading: "Cannot delete book",
        text: "#{jqXHR.responseText}"
    success: (data, textStatus, jqXHR) ->
      $("tr[book_id='#{book_id}").remove()
      $("#bookModal").modal('toggle')

addListeners = ->
  $('tr').on 'click', fill_modal
  $('#edit_book').on 'click', hrefToEdit
  $('#delete_book').on 'click', removeBook

$(document).ready(addListeners)