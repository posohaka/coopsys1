fill_modal = (event) ->
  target = $(this)
  user_id = target.attr 'user_id'
  if undefined != user_id and "" != user_id
    $("#modal_id").text user_id
    $("#modal_name").text target.find("[attr_name='name']").text()
    $("#modal_age").text target.find("[attr_name='age']").text()
    $("#userModal").modal('toggle')

hrefToEdit = (event) ->
  user_id = $("#modal_id").text()
  window.location.replace "#{context_url}/user/?id=#{user_id}"

removeUser = (event) ->
  user_id = $("#modal_id").text()
  $.ajax
    url: "#{context_url}/user/?id=#{user_id}",
    type: 'DELETE'
    error: (jqXHR, textStatus, errorThrown) ->
      $.toast
        icon: 'error',
        hideAfter: 5000,
        heading: "Cannot delete user",
        text: "#{jqXHR.responseText}"
    success: (data, textStatus, jqXHR) ->
      $("tr[user_id='#{user_id}").remove()
      $("#userModal").modal('toggle')

addListeners = ->
  $('tr').on 'click', fill_modal
  $('#edit_user').on 'click', hrefToEdit
  $('#delete_user').on 'click', removeUser

$(document).ready(addListeners)