package com.s3ai.dao;


import com.s3ai.entities.Book;
import com.s3ai.entities.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.UUID;

public class DAO {

    private static DAO dao;

    private DAO() {
    }

    public static DAO getInstance() {
        if (dao == null) {
            dao = new DAO();
        }
        return dao;
    }


    public List<Book> getAllBooks() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        return session.createQuery("select c from Book c", Book.class).getResultList();
    }

    public Book getBookById(UUID id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        return session.createQuery("select c from Book c where c.id = :id", Book.class)
                .setParameter("id", id).getSingleResult();
    }



    public List<User> getAllUsers() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        return session.createQuery("select s from User s", User.class).getResultList();
    }

    public User getUserById(UUID id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        return session.createQuery("select c from User c where c.id = :id", User.class)
                .setParameter("id", id).getSingleResult();
    }

    public void saveEntity(Object obj) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(obj);
        transaction.commit();
        session.close();
    }

    public void updateEntity(Object object) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(object);
        transaction.commit();
        session.close();
    }

    public void deleteEntity(Object object) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(object);
        transaction.commit();
        session.close();
    }
}
