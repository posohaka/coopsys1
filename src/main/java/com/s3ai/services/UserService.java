package com.s3ai.services;

import com.s3ai.dao.DAO;
import com.s3ai.entities.User;

import java.util.List;
import java.util.UUID;

public class UserService {
    private static UserService userService;
    private DAO dao;

    private UserService() {
        this.dao = DAO.getInstance();
    }

    public static UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    public List<User> getAllUsers() {
        return dao.getAllUsers();
    }

    public User getUserById(UUID id) {
        return dao.getUserById(id);
    }

    public void saveUser(User user) {
        dao.saveEntity(user);
    }

    public void updateUser(User user) {
        dao.updateEntity(user);
    }

    public void deleteUser(User user) {
        dao.deleteEntity(user);
    }

    public void fillUserFields(User user, String name, Integer age) {
        user.setName(name);
        user.setAge(age);
    }
}
