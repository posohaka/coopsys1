package com.s3ai.services;

import com.s3ai.dao.DAO;
import com.s3ai.entities.Book;
import com.s3ai.entities.User;

import java.util.List;
import java.util.UUID;

public class BookService {
    private static BookService bookService;
    private DAO dao;

    private BookService() {
        this.dao = DAO.getInstance();
    }

    public static BookService getInstance() {
        if (bookService == null) {
            bookService = new BookService();
        }
        return bookService;
    }

    public List<Book> getAllBooks() {
        return dao.getAllBooks();
    }

    public Book getBookById(UUID id) {
        return dao.getBookById(id);
    }

    public void saveBook(Book book) {
        dao.saveEntity(book);
    }

    public void updateBook(Book book) {
        dao.updateEntity(book);
    }

    public void deleteBook(Book book) {
        dao.deleteEntity(book);
    }

    public void fillBookFields(Book book, User user, String name) {
        book.setUser(user);
        book.setName(name);
    }
}
