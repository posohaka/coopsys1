package com.s3ai.controllers.users;

import com.s3ai.entities.User;
import com.s3ai.services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

public class UsersEdit extends HttpServlet {
    UserService userService = UserService.getInstance();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        User user = null;
        String name = req.getParameter("name") == null ? "" : req.getParameter("name");
        Integer age = Integer.parseInt(req.getParameter("age") == null ? "" : req.getParameter("age"));
        if (null == id || "".equals(id)) {
            user = new User();
            user.setId(UUID.randomUUID());
            userService.fillUserFields(user, name, age);
            userService.saveUser(user);
        } else {
            user = userService.getUserById(UUID.fromString(id));
            userService.fillUserFields(user, name, age);
            userService.updateUser(user);
        }
        resp.sendRedirect("/users");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String idToEdit = req.getParameter("id");
        User user = null;
        RequestDispatcher dispatcher = req.getRequestDispatcher("/pages/users/edit.jsp");
        if (idToEdit != null) {
            user = userService.getUserById(UUID.fromString(idToEdit));
        } else {
            user = new User();
        }
        session.setAttribute("user", user);
        dispatcher.forward(req, resp);
    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (null == id) {
            resp.sendError(420, "Object cannot be found");
            return;
        }
        var user = userService.getUserById(UUID.fromString(id));
        if (null == user) {
            resp.sendError(420, "Object cannot be found");
            return;
        }
        userService.deleteUser(user);
        resp.setStatus(200);
    }
}
