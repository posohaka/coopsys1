package com.s3ai.controllers.users;

import com.s3ai.entities.User;
import com.s3ai.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

public class UsersFill extends HttpServlet {
    UserService userService = UserService.getInstance();

    private String[] names = new String[] {"Jane", "Mary", "Paul", "Jason", "Keanu", "Andrew", "Joseph", "Jotaro", "Ivan", "Jolyne", "Walther"};
    private String[] surnames = new String[] {"Doe", "Reeves", "Statham", "Bourne", "Joestar", "Kujoh", "White", "Van Hallen", "Black", "Smith"};

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        var writer = response.getWriter();
        writer.println("FILLED Instances:");
        var random = new Random();
        for (int i = 0; i < 300; i++) {
            var user = new User();
            userService.fillUserFields(user, randomName(), random.nextInt(60) + 10);
            userService.saveUser(user);
            writer.println(user);
        }
    }

    private String randomName() {
        var random = new Random();
        return String.format("%s %s", names[random.nextInt(names.length)], surnames[random.nextInt(surnames.length)]);
    }
}
