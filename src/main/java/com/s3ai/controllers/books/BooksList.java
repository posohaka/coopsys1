package com.s3ai.controllers.books;

import com.s3ai.entities.Book;
import com.s3ai.services.BookService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class BooksList extends HttpServlet {
    private BookService bookService = BookService.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet");
        RequestDispatcher dispatcher = request.getRequestDispatcher("pages/books/list.jsp");
        request.getSession().setAttribute("books", bookService.getAllBooks());
        dispatcher.forward(request, response);
    }
}
