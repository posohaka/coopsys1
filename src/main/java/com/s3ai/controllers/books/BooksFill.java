package com.s3ai.controllers.books;

import com.s3ai.entities.Book;
import com.s3ai.services.BookService;
import com.s3ai.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

public class BooksFill extends HttpServlet {
    BookService bookService = BookService.getInstance();
    UserService userService = UserService.getInstance();

    private String[] wordsFirst = new String[] {"World", "Great", "Story", "Cute", "Guardians", "Harry Potter", "The Old Man"};
    private String[] wordsSecond = new String[] {"of the", "at", "in", "from", "and"};
    private String[] wordsThird = new String[] {"War", "America", "Russia", "Ocean", "Sea", "Philosopher's Stone", "Deathly Hallows"};

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        var users = userService.getAllUsers();

        var writer = response.getWriter();
        writer.println("FILLED Instances:");
        var random = new Random();
        for (int i = 0; i < 300; i++) {
            var book = new Book();
            bookService.fillBookFields(book, users.get(random.nextInt(users.size())), randomBookName());
            bookService.saveBook(book);
            writer.println(book);
        }
    }

    private String randomBookName() {
        var random = new Random();
        return String.format("%s %s %s", wordsFirst[random.nextInt(wordsFirst.length)], wordsSecond[random.nextInt(wordsSecond.length)], wordsThird[random.nextInt(wordsThird.length)]);
    }
}
