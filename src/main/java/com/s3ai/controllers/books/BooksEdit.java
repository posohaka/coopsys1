package com.s3ai.controllers.books;

import com.s3ai.entities.Book;
import com.s3ai.entities.User;
import com.s3ai.services.BookService;
import com.s3ai.services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class BooksEdit extends HttpServlet {
    BookService bookService = BookService.getInstance();
    UserService userService = UserService.getInstance();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Book book = null;
        String id = req.getParameter("id");
        String userId = req.getParameter("userId") == null ? "" : req.getParameter("userId");
        String name = req.getParameter("name") == null ? "" : req.getParameter("name");

        var user = userService.getUserById(UUID.fromString(userId));

        if (null == user) {
            resp.sendError(500, "Invalid userId");
        }


        if (null == id || "".equals(id)) {
            book = new Book();
            book.setId(UUID.randomUUID());
            bookService.fillBookFields(book, user, name);
            bookService.saveBook(book);
        } else {
            book = bookService.getBookById(UUID.fromString(id));
            bookService.fillBookFields(book, user, name);
            bookService.updateBook(book);
        }
        resp.sendRedirect("/books");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String idToEdit = req.getParameter("id");
        Book book = null;
        RequestDispatcher dispatcher = req.getRequestDispatcher("/pages/books/edit.jsp");
        if (idToEdit != null) {
            book = bookService.getBookById(UUID.fromString(idToEdit));
        } else {
            book = new Book();
        }
        List<User> users = userService.getAllUsers();
        session.setAttribute("users", users);
        session.setAttribute("book", book);
        dispatcher.forward(req, resp);
    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (null == id) {
            resp.sendError(420, "Object cannot be found");
            return;
        }
        var book = bookService.getBookById(UUID.fromString(id));
        if (null == book) {
            resp.sendError(420, "Object cannot be found");
            return;
        }
        bookService.deleteBook(book);
        resp.setStatus(200);
    }
}
