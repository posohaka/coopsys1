package com.s3ai.services;

import com.s3ai.TestUtils;
import com.s3ai.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserServiceTest {

    private UserService userServiceUnderTest;

    @BeforeEach
    void setUp() throws IOException {
        userServiceUnderTest = UserService.getInstance();
        TestUtils.prepareUsers();
    }

    @Test
    void testGetAllUsers() {
        // Setup
        final int expectedResult = 100;

        // Run the test
        final List<User> result = userServiceUnderTest.getAllUsers();

        // Verify the results
        assertEquals(expectedResult, result.size());
    }

    @Test
    void testGetUserById() {
        // Setup
        final User expectedResult = TestUtils.getRandomElement(userServiceUnderTest.getAllUsers());

        // Run the test
        final User result = userServiceUnderTest.getUserById(expectedResult.getId());

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testSaveUser() {
        // Setup
        final User user = TestUtils.getRandomUser();

        // Run the test
        userServiceUnderTest.saveUser(user);

        // Verify the results
        assertEquals(user, userServiceUnderTest.getUserById(user.getId()));

    }

    @Test
    void testUpdateUser() {
        // Setup
        final User user = TestUtils.getRandomElement(userServiceUnderTest.getAllUsers());
        user.setName("TESTING");
        user.setAge(0);
        // Run the test
        userServiceUnderTest.updateUser(user);

        // Verify the results
        assertEquals("TESTING", userServiceUnderTest.getUserById(user.getId()).getName());
        assertEquals(0, userServiceUnderTest.getUserById(user.getId()).getAge());
    }

    @Test
    void testDeleteUser() {
        // Setup
        final User user = TestUtils.getRandomElement(userServiceUnderTest.getAllUsers());
        UUID id = user.getId();

        // Run the test
        userServiceUnderTest.deleteUser(user);

        // Verify the results
        assertThrows(NoResultException.class, () -> userServiceUnderTest.getUserById(id));
    }

    @Test
    void testFillUserFields() {
        // Setup
        final User user = new User();

        // Run the test
        userServiceUnderTest.fillUserFields(user, "name", 0);

        // Verify the results
        assertEquals("name", user.getName());
        assertEquals(0, user.getAge());
    }

    @Test
    void testGetInstance() {
        // Setup
        final UserService expectedResult = UserService.getInstance();

        // Run the test
        final UserService result = UserService.getInstance();

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
