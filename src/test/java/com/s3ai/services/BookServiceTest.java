package com.s3ai.services;

import com.s3ai.TestUtils;
import com.s3ai.entities.Book;
import com.s3ai.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BookServiceTest {

    private BookService ticketServiceUnderTest;

    @BeforeEach
    void setUp() throws IOException {
        ticketServiceUnderTest = BookService.getInstance();
        TestUtils.prepareTickets();
    }

    @Test
    void testGetAllTickets() {
        // Setup
        final int expectedResult = 100;

        // Run the test
        final List<Book> result = ticketServiceUnderTest.getAllBooks();

        // Verify the results
        assertEquals(100, result.size());
    }

    @Test
    void testGetTicketById() {
        // Setup
        final Book expectedResult = TestUtils.getRandomElement(ticketServiceUnderTest.getAllBooks());

        // Run the test
        final Book result = ticketServiceUnderTest.getBookById(expectedResult.getId());

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testSaveTicket() {
        // Setup
        final Book book = TestUtils.getRandomTicket();

        // Run the test
        ticketServiceUnderTest.saveBook(book);

        // Verify the results
        assertEquals(book, ticketServiceUnderTest.getBookById(book.getId()));

    }

    @Test
    void testUpdateTicket() {
        // Setup
        final Book book = TestUtils.getRandomElement(ticketServiceUnderTest.getAllBooks());
        book.setName("TESTING");
        // Run the test
        ticketServiceUnderTest.updateBook(book);


        // Verify the results
        assertEquals("TESTING", ticketServiceUnderTest.getBookById(book.getId()).getName());
    }

    @Test
    void testDeleteTicket() {
        // Setup
        final Book book = TestUtils.getRandomElement(ticketServiceUnderTest.getAllBooks());
        UUID id = book.getId();
        // Run the test
        ticketServiceUnderTest.deleteBook(book);

        // Verify the results
        assertThrows(NoResultException.class, () -> ticketServiceUnderTest.getBookById(id));
    }

    @Test
    void testFillTicketFields() {
        // Setup
        final Book book = new Book();

        final User user = new User();


        // Run the test
        ticketServiceUnderTest.fillBookFields(book, user,"name");

        // Verify the results
        assertEquals(user, book.getUser());
        assertEquals("movie", book.getName());
    }

    @Test
    void testGetInstance() {
        // Setup
        final BookService expectedResult = BookService.getInstance();

        // Run the test
        final BookService result = BookService.getInstance();

        // Verify the results
        assertEquals(expectedResult, result);
    }

}
