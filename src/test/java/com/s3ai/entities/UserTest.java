package com.s3ai.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    private User userUnderTest;

    @BeforeEach
    void setUp() {
        userUnderTest = new User();
    }

    @Test
    void testToString() {
        // Setup
        final String expectedResult = "User{" +
                "id=" + null +
                ", name='" + null + '\'' +
                ", age=" + null +
                '}';

        // Run the test
        final String result = userUnderTest.toString();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testSetId() {
        // Setup
        final UUID uuid = UUID.fromString("9ca6baf1-ce70-4254-98d5-f86d3a2dacab");
        // Run the test
        userUnderTest.setId(uuid);

        // Verify the results
        assertEquals(uuid, userUnderTest.getId());
    }

    @Test
    void testSetName() {
        // Setup

        // Run the test
        userUnderTest.setName("name");

        // Verify the results
        assertEquals("name", userUnderTest.getName());
    }

    @Test
    void testSetAge() {
        // Setup

        // Run the test
        userUnderTest.setAge(0);

        // Verify the results
        assertEquals(0, userUnderTest.getAge());
    }

}
