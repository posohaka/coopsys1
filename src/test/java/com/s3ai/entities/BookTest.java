package com.s3ai.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookTest {

    private Book bookUnderTest;

    @BeforeEach
    void setUp() {
        bookUnderTest = new Book();
    }

    @Test
    void testToString() {
        // Setup
        final String expectedResult = "Book{" +
                "id=" + null +
                ", user=" + null +
                ", movie='" + null + '\'' +
                '}';

        // Run the test
        final String result = bookUnderTest.toString();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testSetId() {
        // Setup
        final UUID uuid = UUID.fromString("cff7b830-9e24-4106-becf-41ce113682bd");
        // Run the test
        bookUnderTest.setId(uuid);

        // Verify the results

        assertEquals(uuid, bookUnderTest.getId());
    }

    @Test
    void testSetUser() {
        // Setup
        final User user = new User();

        // Run the test
        bookUnderTest.setUser(user);

        // Verify the results
        assertEquals(user, bookUnderTest.getUser());
    }


    @Test
    void testSetMovie() {
        // Setup

        // Run the test
        bookUnderTest.setName("movie");

        // Verify the results
        assertEquals("movie", bookUnderTest.getName());
    }
}
