package com.s3ai;

import com.s3ai.dao.HibernateSessionFactory;
import com.s3ai.entities.Book;
import com.s3ai.entities.User;
import com.s3ai.services.BookService;
import com.s3ai.services.UserService;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestUtils {

    public static void prepareTable(String insertsFile, String table) throws IOException {
        var resourceStream = TestUtils.class.getResourceAsStream(insertsFile);
        assertNotNull(resourceStream);
        var insertQueries = IOUtils.toString(resourceStream, StandardCharsets.UTF_8);
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.createSQLQuery("TRUNCATE TABLE " + table + " CASCADE;").executeUpdate();
        session.createSQLQuery(insertQueries).executeUpdate();
        transaction.commit();
        session.close();
    }



    public static void prepareUsers() throws IOException {
        prepareTable("/usersFill.sql", "library_user");
    }

    public static void prepareTickets() throws IOException {
        prepareUsers();
        prepareTable("/ticketsFill.sql", "ticket");
    }

    public static <T> T getRandomElement(List<T> elements) {
        var random = new Random();
        return elements.get(random.nextInt(elements.size()));
    }


    private static String randomName() {
        var random = new Random();
        var names = new String[]{"Jane", "Mary", "Paul", "Jason", "Keanu", "Andrew", "Joseph", "Jotaro", "Ivan", "Jolyne", "Walther"};
        var surnames = new String[]{"Doe", "Reeves", "Statham", "Bourne", "Joestar", "Kujoh", "White", "Van Hallen", "Black", "Smith"};
        return String.format("%s %s", names[random.nextInt(names.length)], surnames[random.nextInt(surnames.length)]);
    }

    public static User getRandomUser() {
        var random = new Random();
        var user = new User();
        var userService = UserService.getInstance();
        userService.fillUserFields(user, randomName(), random.nextInt(60) + 10);
        return user;
    }

    private static String randomMovieName() {
        var random = new Random();
        var wordsFirst = new String[]{"Pirates", "Ladies", "Goblins", "Gangsters", "Programmers", "Hackers", "Wrestlers", "Animals", "Guardians"};
        var wordsSecond = new String[]{"of the", "in", "from"};
        var wordsThird = new String[]{"Galaxy", "America", "Russia", "Japan", "Ocean", "Deep", "Caribbean", "Matrix", "Internet", "Woods", "Caves"};
        return String.format("%s %s %s", wordsFirst[random.nextInt(wordsFirst.length)], wordsSecond[random.nextInt(wordsSecond.length)], wordsThird[random.nextInt(wordsThird.length)]);
    }

    public static Book getRandomTicket() {
        var random = new Random();
        var ticket = new Book();
        var ticketService = BookService.getInstance();
        var userService = UserService.getInstance();
        var users = userService.getAllUsers();
        ticketService.fillBookFields(ticket, users.get(random.nextInt(users.size())), randomMovieName());
        return ticket;
    }
}
