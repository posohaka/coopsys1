package com.s3ai.dao;

import com.s3ai.TestUtils;
import com.s3ai.entities.Book;
import com.s3ai.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DAOTest {

    private DAO daoUnderTest;

    @BeforeEach
    void setUp() throws IOException {
        daoUnderTest = DAO.getInstance();
        TestUtils.prepareTickets();
    }

    @Test
    void testGetAllTickets() {
        // Setup
        final int expectedResult = 100;

        // Run the test
        final List<Book> result = daoUnderTest.getAllBooks();

        // Verify the results
        assertEquals(100, result.size());
    }

    @Test
    void testGetTicketById() {
        // Setup
        final Book expectedResult = TestUtils.getRandomElement(daoUnderTest.getAllBooks());

        // Run the test
        final Book result = daoUnderTest.getBookById(expectedResult.getId());

        // Verify the results
        assertEquals(expectedResult, result);
    }



    @Test
    void testGetAllUsers() {
        // Setup
        final int expectedResult = 100;

        // Run the test
        final List<User> result = daoUnderTest.getAllUsers();

        // Verify the results
        assertEquals(expectedResult, result.size());
    }

    @Test
    void testGetUserById() {
        // Setup
        final User expectedResult = TestUtils.getRandomElement(daoUnderTest.getAllUsers());

        // Run the test
        final User result = daoUnderTest.getUserById(expectedResult.getId());

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testSaveEntity() {
        // Setup
        final User user = TestUtils.getRandomUser();

        // Run the test
        daoUnderTest.saveEntity(user);

        // Verify the results
        assertEquals(user, daoUnderTest.getUserById(user.getId()));
    }



    @Test
    void testDeleteEntity() {
        // Setup
        final Book book = TestUtils.getRandomElement(daoUnderTest.getAllBooks());
        UUID id = book.getId();
        // Run the test
        daoUnderTest.deleteEntity(book);

        // Verify the results
        assertThrows(NoResultException.class, () -> daoUnderTest.getBookById(id));
    }

    @Test
    void testGetInstance() {
        // Setup
        final DAO expectedResult = DAO.getInstance();

        // Run the test
        final DAO result = DAO.getInstance();

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
